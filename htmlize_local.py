import glob
import subprocess
import os
import shutil
import datetime
import re
import pyAgrum as gum


def forhuman(s):
  p = re.compile("c([^p]*)p(.*)")
  return p.sub("Chapter \\1, page \\2", s)


def run(l):
  pythenv = os.environ
  proc = subprocess.Popen(l, stdout=subprocess.PIPE, env=pythenv)
  (stdo, stde) = proc.communicate()
  return stdo, stde


destdir = os.getcwd() + "/html"
if os.path.isdir(destdir):
  shutil.rmtree(destdir)
os.mkdir(destdir)
os.mkdir(destdir+"/notebooks")

items = []
for file in glob.glob("notebooks/*.ipynb"):
  items.append(file)
menu = []
for file in sorted(items):
  print(file)
  run(["jupyter", "nbconvert", "--to=html", "--output=" +
       destdir + "/" + file+".html", file])
  ta = file.split(".")[0].split("-")
  item = forhuman(ta[1])+"&nbsp;:&nbsp;<b>" + \
      (''.join(map(lambda x: x if x.islower() else " "+x, ta[2])))+"</b>"
  menu.append(
      f'<br/><br/><a href="{file}.html" target="mainframe" class="button_class">{item}</a>')

maintenant = datetime.datetime.now()
version = f'<a href="https://agrum.org" target="_blank">pyAgrum</a> : <b>{gum.__version__}</b><br/>generation : <b>{maintenant.strftime("%m/%d/%Y, %H:%M")}</b>'


with open("res/index.tpl", "r") as tpl:
  with open("html/index.html", "w") as home:
    for line in tpl.readlines():
      strip = line.strip()
      if strip == "TABLEOFCONTENT":
        print("\n".join(menu), file=home)
      elif strip == "VERSION":
        print(version, file=home)
      else:
        print(line, file=home, end="")

shutil.copytree(os.getcwd() + "/notebooks/images",
                os.getcwd() + "/html/images")

PUBLISHSERVER = "phw@gate-ia"
PUBLISHDIR = "/web/phw/public_html/aGrUM/BookOfWhy"
run(['ssh', PUBLISHSERVER, 'mkdir -p ' + PUBLISHDIR])
os.chdir('html')
run(['rsync', '-avz', '--timeout=999',
     "-e",
     "ssh -ax -o ClearAllForwardings=yes",
     '.',
     PUBLISHSERVER + ':' + PUBLISHDIR])
print("BookOfWhy published.")
