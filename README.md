# gumBoW

Notebooks for models from " The Book of Why: The New Science of Cause and Effect", Pearl, 2018.

Main authors : **Aymen Merrouche**, PH Wuillemin

Licence : [![Creative Commons Attribution-NonCommercial 4.0 International License](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc/4.0/") 

Test in live : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/agrumery%2Fgumbow/master)

