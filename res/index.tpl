
<html>

<body>
  <style type="text/css">
    body {
      background-color: #EEEEEE !important;
    }
    #foot {
      vertical-align:bottom;
      align:right;
      line-height: 10px;
      position:absolute;
      bottom:0px;
      right:calc(100% - 420px);
      left: 250px;
      font-size:x-small;
      margin-bottom: 3px;
    }
    #buttons_frame {
      line-height: 20px;
      padding-left: 20px;
      padding-top: 10px;
      margin-top: 0px;
      width: 400px;
      height: calc( 100% - 100px);
      text-align: left;
      overflow: auto;
      float: left;
    }

    #mainframe {
      height: 100%;
      width: calc(100% - 420px);
      float: right;
      overflow: auto;
      border: none;
    }


    .button_class {
      -moz-box-shadow: 0px 10px 14px -7px #3e7327;
      -webkit-box-shadow: 0px 10px 14px -7px #3e7327;
      box-shadow: 0px 10px 14px -7px #3e7327;
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #77b55a), color-stop(1, #72b352));
      background: -moz-linear-gradient(top, #77b55a 5%, #72b352 100%);
      background: -webkit-linear-gradient(top, #77b55a 5%, #72b352 100%);
      background: -o-linear-gradient(top, #77b55a 5%, #72b352 100%);
      background: -ms-linear-gradient(top, #77b55a 5%, #72b352 100%);
      background: linear-gradient(to bottom, #77b55a 5%, #72b352 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#77b55a', endColorstr='#72b352', GradientType=0);
      background-color: #77b55a;
      -moz-border-radius: 4px;
      -webkit-border-radius: 4px;
      border-radius: 4px;
      border: 1px solid #4b8f29;
      display: inline-block;
      cursor: pointer;
      color: #ffffff;
      font-family: Arial;
      font-size: 13px;
      font-weight: bold;
      padding: 6px 12px;
      text-decoration: none;
      text-shadow: 0px 1px 0px #5b8a3c;
      width: 350px;
    }

    .button_class:hover {
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #72b352), color-stop(1, #77b55a));
      background: -moz-linear-gradient(top, #72b352 5%, #77b55a 100%);
      background: -webkit-linear-gradient(top, #72b352 5%, #77b55a 100%);
      background: -o-linear-gradient(top, #72b352 5%, #77b55a 100%);
      background: -ms-linear-gradient(top, #72b352 5%, #77b55a 100%);
      background: linear-gradient(to bottom, #72b352 5%, #77b55a 100%);
      filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#72b352', endColorstr='#77b55a', GradientType=0);
      background-color: #72b352;
    }

    .button_class:active {
      position: relative;
      top: 1px;
    }

    # container {
      width: 100%;
      margin: 0px auto;
    }
  </style>

  <div id="container">

    <div id="buttons_frame">
<a href="http://bayes.cs.ucla.edu/WHY/" target="_blank"><img src="http://bayes.cs.ucla.edu/WHY/Pearl-The-Book.jpg" width="80" align="left" valign="middle"></a><small>
<p>The notebooks proposed here present some of the examples of the <a href="http://bayes.cs.ucla.edu/WHY/">book of Why</a> from Judea Pearl and Dana Mackenzie. </p>
<p>The notebooks are written with the <a href="https://agrum.org">PyAgrum</a>'s python module.</p></small>
TABLEOFCONTENT
          <br />&nbsp;
        </div>
        <div id="foot">
VERSION
        </div>

<!-- Google Analytics -->
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-97418814-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- End Google Analytics -->
    </div>

    <iframe id="mainframe" name="mainframe" src=""
      allowtransparency="true"></iframe>
  </div>
</body>

</html>

